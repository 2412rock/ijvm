
The tests can be found in src/test/java/pad/ijvm/

To build the project:
```bash
./gradlew build
```

To run:
```bash
./gradlew run
```




