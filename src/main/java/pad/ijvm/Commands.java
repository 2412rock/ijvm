package pad.ijvm;

/**
 * Created by adi on 22-6-17.
 */
public enum Commands {
    IFEQ("IFEQ", -103, 2), //0x99
    GOTO("GOTO", -89 , 2),
    IOR("IOR", -80, 0),
    BIPUSH("BIPUSH", 0x10, 1),
    DUP("DUP", 89, 0), //0x59
    ERR("ERR", 0xFE, 0),
    HALT("HALT", -1, 0),
    IADD("IADD", 0x60, 0),
    IAND("IAND", 0x7E, 0),
    IFLT("IFLT", -101, 2),
    IF_ICMPEQ("IF_ICMPEQ", -97, 2),
    IINC("IINC", -124, 2),
    ILOAD("ILOAD", 0x15, 1),
    IN("IN", 0xFC, 0),
    INVOKEVIRTUAL("INVOKEVIRTUAL", -74, 2),
    IRETURN("IRETURN", -84, 0),
    ISTORE("ISTORE", 0x36, 1),
    ISUB("ISUB", 0x64, 0),
    LDC_W("LDC_W", 0x13, 2),
  //  NOP("NOP", 0x00, 0),
    OUT("OUT", -3, 0),
    POP("POP", 0x57, 0),
    SWAP("SWAP", 0x5F, 0),
    WIDE("WIDE", 0xC4, 0);

    private final String label;
    private final int command;
    private final int nrOfArgs;


    Commands(String label, int command, int nrOfArgs) {
        this.label = label;
        this.command = command;
        this.nrOfArgs = nrOfArgs;
    }

    public static int getArgumentsNumber(int command){
        for(Commands i: Commands.values()){
            if(i.command == command){
                return i.nrOfArgs;
            }
        }
        return -1;
    }

    public static int getCommand(String label){
        for(Commands i: Commands.values()){
            if(i.label == label){
                return i.command;
            }
        }
        return -1;
    }

    public static String getLabel(int command){
        for(Commands i: Commands.values()){
            if(i.command == command){
                return i.label;
            }
        }

        return "";
    }


    public static boolean isOPCode(int command){

        for(Commands i: Commands.values()){
           //System.out.println("We compare: " + command + " with: " + i.command);
            if(Integer.compare(command,i.command)==0){
                return true;
            }
        }
        return false;
    }



}
