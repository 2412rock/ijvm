package pad.ijvm;

import pad.ijvm.interfaces.IJVMInterface;

import java.io.InputStream;
import java.io.PrintStream;
import java.nio.ByteBuffer;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.NoSuchElementException;


/**
 * Created by r0ck on 6/11/2017.
 */
public class IJVM implements IJVMInterface{
    // BIPUSH: 0X10

    PrintStream out;
    private int PC,frameCounter;
    Word word;
    Text text;
    BinaryLoader parsedInput;
    //Stack stack;
    Deque<Word> stack;
    //Frame frame;
    boolean isHalted = false;
    Deque<Frame> frameStack;
    int lockPC = 10000;
    int globalLock;

    //for now: step, run, getText, getPC, getInstruction

    IJVM(byte[] bytes){
        PC = 0;
        parsedInput = new BinaryLoader(bytes);
        setOutput(System.out);
        //frame = new Frame();
        text = parsedInput.readText();
        stack = new ArrayDeque<Word>();
        frameStack = new ArrayDeque<>();
        frameStack.push(new Frame());
        frameCounter = 0;
        globalLock = 0;
    }

    @Override
    public void step() {
            //out.println();
          //out.println("PC: " + PC);
        try {
            //out.println("------TOP OF THE STACK: " + topOfStack());
        }
        catch (NullPointerException e){}
            String command = new String(text.getCommandAndArgs(PC).getLabel());
            //out.println(command);
            switch (command) {
                case "BIPUSH":
                    int arg = text.getCommandAndArgs(PC).getArgument(0);
                    Word result = new Word(arg);
                    stack.push(result);
                  //  out.println("BIPUSH Arg: " + arg);
                  //  out.println("TOP OF STACK " + topOfStack());
                    PC += 1;
                    break;

                case "IADD":
                    int word1 = stack.pop().getInt();
                    int word2 = stack.pop().getInt();
                    int sum = word1 + word2;
                    result = new Word(sum);
                    stack.push(result);
                   // out.println("IADD result: " + sum);
                    PC += 1;
                    break;

                case "ISUB":
                    word1 = stack.pop().getInt();
                    word2 = stack.pop().getInt();
                    int sub = -(word1 - word2);
                    result = new Word(sub);
                    stack.push(result);
                   // out.println("ISUB result: " + sub);
                    //out.println("TOP OF THE STACK: " + topOfStack());
                    PC += 1;
                    break;

                case "IAND":
                    word1 = stack.pop().getInt();
                    word2 = stack.pop().getInt();
                    int and = word1 & word2;
                    result = new Word(and);
                    stack.push(result);
                   // out.println("IAND result: " + and);
                    PC += 1;
                    break;

                case "IOR":
                    word1 = stack.pop().getInt();
                    word2 = stack.pop().getInt();
                    int or = word1 | word2;
                    result = new Word(or);
                    stack.push(result);
                    //out.println("IOR result: " + or);
                    PC += 1;
                    break;

                case "POP":
                    int top = stack.pop().getInt();
                   // out.println("POPed: " + top);
                    PC += 1;
                    break;

                case "SWAP":
                    Word word_1 = stack.pop();
                    Word word_2 = stack.pop();
                    stack.push(word_1);
                    stack.push(word_2);
                    PC += 1;
                    break;

                case "GOTO":
                    byte[] resultBytes = new byte[2];
                    resultBytes[0] = text.getCommandAndArgs(PC).getArgument(0);
                    resultBytes[1] = text.getCommandAndArgs(PC).getArgument(1);
                    // out.println("RESULT BYTE[0]" + resultBytes[0]);
                    //out.println("RESULT BYTE[1]" + resultBytes[1]);
                    int offset;
                    if (resultBytes[0] < 0 | resultBytes[1] < 0) {
                        offset = ((resultBytes[0] << 8) | (resultBytes[1]));
                    } else {
                        offset = ((resultBytes[0] & 0xff) << 8) | (resultBytes[1] & 0xff);
                    }
                    //out.println("PC BEFORE BRANCH: " + PC + " OFFSET: " + offset);
                    //text.getByteIndex(PC,offset);
                    PC = text.getPCAtByteIndex(text.getByteIndex(PC, offset));//offset is in bytes
                    break;
                //PC = text.getPCatByte(PC,offset);//increase next 6 bytes

                case "OUT":
                    out.print((char)stack.pop().getInt());
                    PC += 1;
                    break;

                case "DUP":
                    int topWord = stack.peek().getInt();
                    result = new Word(topWord);
                    stack.push(result);
                    PC += 1;
                   // out.println("DUP " + topWord);
                    break;

                case "IFEQ":
                    int check = stack.pop().getInt();
                 //  out.println("IFEQ CHECK TOP VALUE: " + check);

                    if (check == 0) {
                        //out.println("IFEQ");
                        resultBytes = new byte[2];
                        resultBytes[0] = text.getCommandAndArgs(PC).getArgument(0);
                        resultBytes[1] = text.getCommandAndArgs(PC).getArgument(1);

                        if (resultBytes[0] < 0 | resultBytes[1] < 0) {
                            offset = ((resultBytes[0] << 8) | (resultBytes[1]));
                        } else {
                            offset = ((resultBytes[0] & 0xff) << 8) | (resultBytes[1] & 0xff);
                        }
                        PC = text.getPCAtByteIndex(text.getByteIndex(PC, offset));//offset is in bytes
                       //out.println("IFEQ");
                    } else {
                        PC += 1;
                    }
                    break;


                case "IFLT":
                    check = stack.pop().getInt();
                   out.println("IFLT CHECK TOP VALUE: " + check);

                    if (check < 0) {
                        //out.println("IFEQ");
                        resultBytes = new byte[2];
                        resultBytes[0] = text.getCommandAndArgs(PC).getArgument(0);
                        resultBytes[1] = text.getCommandAndArgs(PC).getArgument(1);

                        if (resultBytes[0] < 0 | resultBytes[1] < 0) {
                            offset = ((resultBytes[0] << 8) | (resultBytes[1]));
                        } else {
                            offset = ((resultBytes[0] & 0xff) << 8) | (resultBytes[1] & 0xff);
                        }
                        PC = text.getPCAtByteIndex(text.getByteIndex(PC, offset));//offset is in bytes
                       // out.println("IFLT");
                    } else {
                        PC += 1;
                    }
                    break;

                case "HALT":
                    isHalted = true;
                    out.println("HALT");
                    break;

                case "IF_ICMPEQ":
                    word1 = stack.pop().getInt();
                    word2 = stack.pop().getInt();

                    if (word1 == word2) {
                        resultBytes = new byte[2];
                        resultBytes[0] = text.getCommandAndArgs(PC).getArgument(0);
                        resultBytes[1] = text.getCommandAndArgs(PC).getArgument(1);

                        if (resultBytes[0] < 0 | resultBytes[1] < 0) {
                            offset = ((resultBytes[0] << 8) | (resultBytes[1]));
                        } else {
                            offset = ((resultBytes[0] & 0xff) << 8) | (resultBytes[1] & 0xff);
                        }
                        PC = text.getPCAtByteIndex(text.getByteIndex(PC, offset));//offset is in bytes
                       // out.println("IF_ICMPEQ");
                    } else {
                        PC += 1;
                    }
                    break;

                case "LDC_W":
                    resultBytes = new byte[2];
                    resultBytes[0] = text.getCommandAndArgs(PC).getArgument(0);
                    resultBytes[1] = text.getCommandAndArgs(PC).getArgument(1);
                    offset = ((resultBytes[0] & 0xff) << 8) | (resultBytes[1] & 0xff);
                    int constant = getConstant(offset);
                    result = new Word(constant);
                    stack.push(result);
                    //out.println("LDC_W: " + result.getInt());
                    PC += 1;
                    break;

                case "ILOAD":
                    //push LV onto the stack.
                   // out.println("ILOAD");
                    int LVindex = text.getCommandAndArgs(PC).getArgument(0);
                    //out.println("LVINDEX: " +LVindex );
                    //int plm =  frameStack.peek().getLocalVariable(0);
                    int localVar = frameStack.peek().LV.get(LVindex);
                   // out.println("LOADED: " + localVar);
                        result = new Word(localVar);
                    stack.push(result);

                    //OFFSET IN THE LOCAL VARIABLE FRAME
                    PC += 1;
                    //out.println("PUSH: index: " + LVindex + " VAR: " + localVar );
                    break;

                case "ISTORE":
                    //pop word from stack and store it in LV
                    //frame = new Frame();
                    word1 = stack.pop().getInt();

                    int varIndex = text.getCommandAndArgs(PC).getArgument(0);
                    //frame.addLocalVariable(word1);

                    frameStack.peek().setLocalVariable(varIndex,word1);

                    //out.println("--SET VARIABLE: " + varIndex + " VAL: " + word1);
                    PC += 1;
                    //frameCounter += 1;
                    break;

                case "IINC":
                    //use ArrayList
                    resultBytes = new byte[2];
                    resultBytes[0] = text.getCommandAndArgs(PC).getArgument(0);
                    resultBytes[1] = text.getCommandAndArgs(PC).getArgument(1);
                    frameStack.peek().setIINC(resultBytes[0] ,resultBytes[1]);
                    PC += 1;
                    break;

                case "INVOKEVIRTUAL":
                  //  out.println("INVOKEVIRTUAL:");
                   //OBJ ref ???
                    int argument1 = stack.pop().getInt();
                    int argument2 = stack.pop().getInt();
                    resultBytes = new byte[2];
                    resultBytes[0] = text.getCommandAndArgs(PC).getArgument(0);
                    resultBytes[1] = text.getCommandAndArgs(PC).getArgument(1);
                    int offsetInPool = ((resultBytes[0] & 0xff) << 8) | (resultBytes[1] & 0xff);//CP reference


                   int _offset = getConstant(offsetInPool);//first address in bytes in text

                    byte[] ARGSSIZE = new byte[2];
                    ARGSSIZE[0] = text.instructionBytes[_offset];
                    //out.println("ARG: " + ARGSSIZE[0]);
                    _offset += 1;
                    ARGSSIZE[1] = text.instructionBytes[_offset];
                   // out.println("ARG: " + ARGSSIZE[1]);
                    _offset += 1;

                    int nrOfArgs = ((ARGSSIZE[0] & 0xff) << 8) | (ARGSSIZE[1] & 0xff) - 1;
                   // System.out.println("nrOfArgs: " + nrOfArgs);
                    //read next 2 bytes in text = LV size
                    byte[] LVBYTES = new byte[2];
                    LVBYTES[0] = text.instructionBytes[_offset];
                    _offset += 1;
                    LVBYTES[1] = text.instructionBytes[_offset];
                    _offset += 1;
                    int LVsize = ((LVBYTES[0]  & 0xff) << 8) | (LVBYTES[1] & 0xff);

                    globalLock = _offset;
                //    out.println("HERE" + globalLock);
                    //read instruction after offset + 4
                    int framePC = text.getPCAtByteIndex(_offset);

                    //contiunue getting bytes: 1OBJ ref, arg1,arg2,
                    Frame frame = new Frame(PC,nrOfArgs,0);

                    PC = framePC;
                    if(nrOfArgs == 1) {
                        //rgument1 = stack.pop().getInt();
                        frame.addArgument(0);
                        frame.addArgument(argument1);
                    }
                    if(nrOfArgs == 2) {
                        frame.addArgument(0);
                       // frame.addArgument(argument1);
                        frame.addArgument(argument2);
                        frame.addArgument(argument1);
                    }

                    frameStack.push(frame);

                    break;

                case "IRETURN":
                    //System.out.println("IRETURN");
                    PC = frameStack.peek().getOldPC() + 1;
                    //PC in frame points only to its own stack
                    //PC in main also points to previous frame stack
                    //convert global PC to localPC
                    //stop PC at begining of first instruction byte index
                    //Lock PC at 5
                    //out.println("LOCKPC: " + text.getPCAtByteIndex(globalLock));
                    lockPC = text.getPCAtByteIndex(globalLock);
                   // out.println("LOCKPC + " + lockPC);
                    frameStack.pop();
                    break;
            }

       // PC += 1;
    }

    @Override
    public void run() {
            int counter = 0;

           // System.out.println("NUMBER OF COMMANDS: " + text.getNumberOfCommands());
            while(text.thereisNextOP(PC) && !isHalted && PC != lockPC) {
                //out.println("PC" + PC);
                counter += 1;
                step();
            }


    }

    @Override
    public byte[] getText() {
        return text.instructionBytes;
    }

    @Override
    public int getProgramCounter() {
        return text.getByteIndex(PC,0);
    }

    @Override
    public byte getInstruction() {
        return text.getCommandAndArgs(getProgramCounter()).getCommand();
    }

    @Override
    public int topOfStack() {

        return stack.peek().getInt();
    }

    @Override
    public int[] getStackContents() {
        return new int[0];
    }

    @Override
    public int getLocalVariable(int i) {

        return frameStack.peek().getLocalVariable(i);
    }

    @Override
    public int getConstant(int i) {
        return parsedInput.readPool().returnConstant(i);
    }

    @Override
    public void setInput(InputStream in) {

    }

    @Override
    public void setOutput(PrintStream out) {
        this.out = out;
        //out.println();

    }
}