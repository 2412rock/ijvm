package pad.ijvm;

import java.nio.ByteBuffer;

/**
 * Created by adi on 18-6-17.
 */
public  class BinaryLoader {


    Pool pool;
    Text text;
    private int currentOffset;
    static final int wordLen = 4;
    byte[] bytes;

    BinaryLoader(byte[] bytes) {
        this.bytes = bytes;
        pool = new Pool();
        passPoolData();
        passInstructionsData();
        currentOffset = 0;
    }


    Pool readPool() {
        return pool;
    }

    Text readText() {
        return text;
    }

    private Word writeWord(int offSet, int size) {

        byte[] resultBytes = new byte[size];//size in bytes 1 word = 4 bytes
        int counter = 0;
        for (int i = offSet; i < offSet + size; i++) {
            resultBytes[counter] = bytes[i];
            counter += 1;// bytes[0], bytes
        }
        //currentOffset += 4;
        return new Word(resultBytes);
        //magicNumber = Word.wordToInt32(resultBytes,0,4);
    }

    int getMagicNumber() {
        return writeWord(0, 4).wordToInt32();


    }

    void passPoolData() {
        currentOffset += 4;
        pool.setOrigin(writeWord(currentOffset, 4).wordToInt32());
        currentOffset += 4;
        pool.setSize(writeWord(currentOffset, 4).wordToInt32());
        currentOffset += 4;
        pool.initConstants();

        for (int i = 0; i < pool.getNumberOfConstants(); i++) {
            pool.setConstant(writeWord(currentOffset, 4).wordToInt32());
            currentOffset += 4;
        }
    }

    void passInstructionsData() {
        text = new Text(writeWord(currentOffset, 4).wordToInt32(), writeWord(currentOffset + 4, 4).wordToInt32());
        currentOffset += 8;
        text.addOPcodes(writeWord(currentOffset, text.size).bytes);

    }
}

   // Instruction readText(){
  //      program_counter += 1;
   //     return instruction[program_counter];
//

     /*  int readInt(int offSet, int size){// size of the Int in bytes
        //returns magic number

        byte[] resultBytes = new byte[size];//size in bytes 1 word = 4 bytes
        int counter = 0;
        for(int i=offSet; i<offSet+size;i++){
            resultBytes[counter] = bytes[i];
            counter += 1;// bytes[0], bytes
        }
        return ByteBuffer.wrap(resultBytes, 0, counter).getInt();
    }*/






