package pad.ijvm;

import pad.ijvm.interfaces.IJVMInterface;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

public class MachineFactory {



    public static IJVMInterface createIJVMInstance(File binaryFile) throws IOException {
        // Create new machine instance here and return it.

        // 1) Load the binary
        byte[] bytes = new byte [(int) binaryFile.length()];
        System.out.println(binaryFile.length());
        FileInputStream fileInputStream = new FileInputStream(binaryFile);
        fileInputStream.read(bytes);
        fileInputStream.close();




        IJVM machine = new IJVM(bytes);

        // 2) Return the new IJVM instance without starting it.

        return machine;
    }

}