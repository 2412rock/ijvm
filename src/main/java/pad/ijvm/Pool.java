package pad.ijvm;

import java.nio.ByteBuffer;

/**
 * Created by adi on 18-6-17.
 */
public class Pool {
//parsing is done here

    static final int wordSize = 4;
     int origin,size;
    int[] constants;
    int constantPointer;

    Pool(){
        constantPointer = 0;
    }

    void setSize(int size){
        this.size = size;
    }

    void setOrigin(int origin){
        this.origin = origin;
    }
    int getOrigin(){
        return this.origin;
    }
    int getSize(){
        return this.size;
    }

    int getNumberOfConstants(){
        return getSize()/wordSize;
    }
    void initConstants(){
        constants = new int[getSize()];
    }
    void setConstant(int constant){
        constants[constantPointer] = constant;
        constantPointer += 1;
    }

    int returnConstant(int atIndex){
        return constants[atIndex];
    }



}
