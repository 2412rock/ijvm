package pad.ijvm;

/**
 * Created by adi on 22-6-17.
 */
public class Command {

    byte command;
    String label;
    byte[] args;
    int nrOfArgs;

    Command(byte command, int nrOfArgs,String label){
        this.command = command;
        args = new byte[nrOfArgs];
        this.nrOfArgs = 0;
        this.label = label;
    }


    void addArg(byte argument){
        this.args[nrOfArgs] = argument;
        nrOfArgs += 1;

    }

    int getNumberOfArgs(){
        return nrOfArgs;
    }
    void execute(){

    }

    byte getCommand(){
        return command;
    }

    byte getArgument(int index){
        return args[index];
    }

    String getLabel(){
        return this.label;
    }
}
