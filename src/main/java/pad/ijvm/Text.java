package pad.ijvm;

import java.nio.ByteBuffer;

/**
 * Created by r0ck on 6/20/2017.
 */
public class Text {

    byte[] instructionBytes;
    private int commandPointer;
    int origin, size;
    int[] byteIndex;
    Command[] commands;


    Text(int origin, int size) {
        this.origin = origin;
        this.size = size;
        commandPointer = 0;
        commands = new Command[200];
        byteIndex = new int[200];
    }

    void addOPcodes(byte[] bytes) {
        instructionBytes = bytes;

        for (int i = 0; i < bytes.length; i++) {
            //System.out.println("i:: " + i);
            //save index for args wich will be the current byte

            if (Commands.isOPCode(bytes[i])) {
                //savebyteIndex
                byte temp = bytes[i];
                int numberOfArgs = Commands.getArgumentsNumber(instructionBytes[i]);
                String label = Commands.getLabel(instructionBytes[i]);
               //System.out.println("Value: 0x" + Word.byteToHex(temp) + " is an instruction " + "NR OF ARGS: " + numberOfArgs );
                commands[commandPointer] = new Command(bytes[i], numberOfArgs,label);
                byteIndex[commandPointer] = i;
               // System.out.println( commands[commandPointer].label+ " is an instruction " + "NR OF ARGS: " + numberOfArgs );

                if (numberOfArgs == 1) {
                    i += 1;
                    commands[commandPointer].addArg(bytes[i]);

                   // System.out.println("Value:" + Word.byteToHex(bytes[i]) +" is the argument");

                }
                else if (numberOfArgs == 2) {

                    i++;
                    commands[commandPointer].addArg(bytes[i]);
                    i++;
                    commands[commandPointer].addArg(bytes[i]);
                    //System.out.println("XOMM: " + Commands.getLabel(bytes[i-2]));
                    ///if(Commands.getLabel(bytes[i-2]) == Commands.getLabel(-74)){
                       // i += 4;
                    //}
                   // System.out.println("Values:" + Word.byteToHex(bytes[i]) + "," +  Word.byteToHex(bytes[i-1]) +" are the arguments");
                }

                commandPointer += 1;
            }


        }

    }

    int readNext2BytesInt(int offset){
        System.out.println("LA MA-TA" + instructionBytes.length);
       return ByteBuffer.wrap(instructionBytes, offset, 2).getInt();

    }

    int getByteIndex (int currentCommandPointer, int byteOffset){
        //next command will be at index x. This needs to be translated to commandPointer.

            return byteIndex[currentCommandPointer] + byteOffset;

            //getCommandPointerAt(byteIndex[currentPC]);
    }

    int getPCAtByteIndex(int _getbyteIndex){

        //if(bytes[_getbyteIndex] is valid instruction  ){
            for(int tempPC=0; tempPC<getNumberOfCommands(); tempPC++){
                //System.out.println("INDEX:" + _getbyteIndex);
                if(byteIndex[tempPC] == _getbyteIndex){
                    //System.out.println("COMANDA ESTE: " + getCommandAndArgs(tempPC).label);
                    return tempPC;
                }


            }
            return -1;
        //}
    }

    void setNewNumberOfCommands(int substract){
        commandPointer -= substract-1;
    }
    int getNumberOfCommands(){
        return commandPointer;
    }

    Command getCommandAndArgs(int index) {
        return commands[index];
    }

    boolean thereisNextOP(int currentPC){
        return currentPC < getNumberOfCommands();
    }

}