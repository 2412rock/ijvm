package pad.ijvm;

import java.nio.ByteBuffer;

/**
 * Created by adi on 18-6-17.
 */
public class Word {

    byte[] bytes;
    int integer;

    Word(byte[] bytes){
        this.bytes = bytes;
    }

    Word(int i){
        this.integer = i;
    }


   public int wordToInt(){
       return ByteBuffer.wrap(bytes, 0, bytes.length).getInt();
    }
    public static String byteToHex(byte bytes){
        StringBuilder sb = new StringBuilder();
        sb.append(String.format("%02X ", bytes));

        return sb.toString();
    }

    int getInt(){
        return this.integer;
    }



    public static byte[] intToBytes(int number, int size) {

        byte[] bytes = new byte[size];
        for (int i = 0; i < size; i++) {
            bytes[i] = (byte) (number >>> (i * 8));
        }
        return bytes;
    }

    public static Word createWord( int _byte) {

        return new Word(_byte);
        //magicNumber = Word.wordToInt32(resultBytes,0,4);
    }

    int wordToInt32() {
        return ByteBuffer.wrap(bytes, 0, 4).getInt();
    }


}

