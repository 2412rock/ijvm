package pad.ijvm;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.List;

/**
 * Created by r0ck on 6/26/2017.
 */
public class Frame {
    //contains the local variable of the current frame
    List<Integer> LV;
    int LVPointer, backupPC, nrOfArgs, LVsize,argCounter;
    int[] argument;
    Deque<Word> stack;


    Frame(int backupPC , int nrOfArgs, int LVsize){
        this.backupPC = backupPC;
        this.nrOfArgs = backupPC;
        this.LVsize = LVsize;
        LV = new ArrayList<>();
        LVPointer = 0;
        argCounter = 0;
        stack = new ArrayDeque<Word>();
        argument = new int[nrOfArgs];
    }

    Frame(){
        LV = new ArrayList<>();
        LVPointer = 0;
    }

    void addLocalVariable(int word){
        //LVPointer += 1;
      //  stack.push(word);
        LV.add(word);
        //LV[LVPointer] = word;
        LVPointer += 1;
    }

    void addArgument(int argument){
        //this.argument[argCounter] = argument;
        LV.add(argument);
    }

    void setLocalVariable(int index, int value){
        int newValue;
        try {
            newValue = value;
            LV.set(index,newValue);
        }
        catch (IndexOutOfBoundsException e) {
            newValue = value;
            addLocalVariable(newValue);
        }
    }

    void setIINC(int index, int value){
        int newValue;
        try {
            int temp = getLocalVariable(index);
            newValue = temp + value;
            LV.set(index,newValue);
        }
        catch (IndexOutOfBoundsException e) {
            newValue = value;
            addLocalVariable(newValue);
        }

    }

    int getLocalVariable(int index){
        return LV.get(index);
    }

    int getOldPC(){
        return backupPC;
    }

}
